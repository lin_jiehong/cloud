-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `baas2_log` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `baas2_log`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `debug`;
CREATE TABLE `debug` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` text,
  `message` text,
  `info` text,
  `device` text,
  `system` text,
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `method` text,
  `url` text,
  `status` int(11) NOT NULL DEFAULT '200',
  `time` text,
  `length` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2017-11-03 08:01:31

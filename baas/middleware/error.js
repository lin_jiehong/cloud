const _ = require("lodash");
const util = require("./../util/util");

module.exports = () => {
  return async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      console.error(err);
      if (!_.isEmpty(ctx.baas)) {
        // 发布订阅
        BaaS.redis.publish(
          util.getVersionKey("log"),
          JSON.stringify(
            Object.assign(ctx.baas, {
              message: err.name + " : " + err.message
            })
          )
        );
      }

      if (401 !== ctx.status) {
        ctx.status = 500;
      }
      ctx.error(err.name + " : " + err.message);
    }
  };
};

wemall商城&koahub商城所有系统将于4月底或5月初开放免费下载，具体请联系客服咨询
![输入图片说明](https://images.gitee.com/uploads/images/2020/0416/161820_09b5bb90_84549.png "屏幕截图.png")

# cloud

### 介绍
一站式后端云服务解决方案。为Web、小程序、 APP等应用，
提供高效、简单、安全的后端云服务支持。支持一键生成源码、私有云等功能。

### 文档
[https://cloud.qingful.com/doc/#/zh-cn/summary](https://cloud.qingful.com/doc/#/zh-cn/summary)


### 目录结构
- baas 核心api接口
- baas-dashboard 后台管理服务端
- baas-official 官网
- baas-proxy 代理
- baas-www 后台管理浏览器端

### 部署
导入根目录下的db.sql

#### baas
1. 配置config.js文件
2. 执行命令yarn && npm start启动

#### baas-dashboard
1. 配置config.js文件
2. 执行命令yarn && npm start启动

#### baas-www
1. 修改baas-www/src/util.js axios.defaults.baseURL
2. 执行yarn && npm run dev启动

### 重要
此项目我们已经停止运营，所以开源出来，仅供大家研究学习。

![介绍](https://images.gitee.com/uploads/images/2020/0229/170838_5e2caf08_84549.jpeg "介绍")
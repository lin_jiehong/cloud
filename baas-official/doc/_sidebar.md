- 概述
  - [简介](zh-cn/summary.md)

- 开始使用
  - [快速开始](zh-cn/quickstart.md)
  - [数据表](zh-cn/table.md)
  - [云函数](zh-cn/cloudFunction.md)
  - [难点解答](zh-cn/difficultAnswers.md)
  - [收费标准](zh-cn/feeScale.md)

- 云插件
  - 公众号
	  - [快速开始](zh-cn/wx/quickstart.md)
	  - [微信网页授权登录](zh-cn/wx/auth.md)
	  - [微信支付](zh-cn/wx/pay.md)
  - 小程序
    - [快速开始](zh-cn/wxa/quickstart.md)
    - [用户信息](zh-cn/wxa/user.md)
    - [模版消息](zh-cn/wxa/templates.md)
    - [客服消息](zh-cn/wxa/message.md)
    - [素材管理](zh-cn/wxa/media.md)
    - [小程序码](zh-cn/wxa/qrcode.md)
    - [小程序支付](zh-cn/wxa/pay.md)
    - [附近](zh-cn/wxa/nearby.md)
    - [数据统计与分析](zh-cn/wxa/dataCube.md)
  - [支付宝支付](zh-cn/plugins/alipay.md)
  - [短信验证码](zh-cn/plugins/smsCaptcha.md)
  - [图片验证码](zh-cn/plugins/pictureCaptcha.md)
  - [邮件](zh-cn/plugins/mail.md)

- 调试
  - [小程序](zh-cn/debug/wxa.md)
  - [浏览器](zh-cn/debug/javascript.md)
  
- [常见问题](zh-cn/commonProblem.md)
